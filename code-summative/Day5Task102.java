class AllData extends DataSekolah {
    private String id;
    private String nama;
    private String email;
    private long nilai = 0;

    public AllData(String id, String nama, String email) {
        this.id = id;
        this.nama = nama;
        this.email = email;
    }

    public AllData(String id, String nama, String email, long nilai) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.nilai = nilai;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addNilai(long nilai) {
        this.nilai += nilai;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getEmail() {
        return email;
    }

    public long getNilai() {
        return nilai;
    }

    public static void main(String[] args) {
        System.out.println("DataBase Sekolah");
        DataSekolah saving = new AllData("001002", "Michael Jones", "mandalarelix3@gmail.com", 98);

        System.out.println(saving.getNama() + "'s ID : " + saving.getId());

        System.out.println(saving.getNama() + "'s Email : " + saving.getEmail());

        System.out.println(saving.getNama() + "'s Nilai : " + saving.getNilai());
    }
}
