import java.util.Scanner;

public class Day5Task3 {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int angka;

        System.out.print("Masukan Angka kesukaan anda: ");
        angka = input.nextInt();

        switch (angka) {
            case 1:
                System.out.println("Angka 1");
                break;
            case 2:
                System.out.println("Angka 2");
                break;
            case 3:
                System.out.println("Angka 3");
                break;
            case 4:
                System.out.println("Angka 4");
                break;
            case 5:
                System.out.println("Angka 5");
                break;
            default:
                System.out.println("Lebih besar/kecil dari 5");
                break;
        }
    }
}
