
abstract class DataSekolah {
    abstract void setId(String id);

    abstract void setNama(String nama);

    abstract void setEmail(String email);

    abstract String getId();

    abstract String getNama();

    abstract String getEmail();

    abstract long getNilai();

}
