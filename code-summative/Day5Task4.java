import java.util.Scanner;

public class Day5Task4 {

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int angka;

        System.out.print("Masukan Angka yang ingin dihitung: ");
        angka = input.nextInt();

        for (int i = 1; i <= angka; i++) {
            System.out.println(i);
        }
    }
}