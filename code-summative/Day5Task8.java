import java.util.ArrayList;

public class Day5Task8 {
    public static void main(String[] args) {
        ArrayList<String> negara = new ArrayList<String>();
        System.out.println("Nama Negara <ArrayList>: ");
        negara.add("Indonesia");
        negara.add("Malaysia");
        negara.add("Singapura");
        negara.add("Laos");
        negara.add("Myanmar");

        for (String i : negara)
            System.out.println(i);
    }
}
